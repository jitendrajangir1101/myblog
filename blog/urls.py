
from django.urls import path , include
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    
    path('',views.blog_home , name='blog_home'),
    path('<slug:slug>/' , views.blog_page, name='blog_page') , 
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)