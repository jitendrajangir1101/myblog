from django.db import models

class blog_model(models.Model):
    title=models.CharField(max_length=500)
    post=models.CharField(max_length=500)
    image=models.ImageField(upload_to='images/')
    file_link=models.CharField(max_length=500)
    pub_name=models.CharField(max_length=500)
    pub_date=models.DateTimeField()
    slug=models.SlugField(max_length=500)
    
    def __str__(self):
        return self.title
    def snipet(self):
        return self.post[:150] + "......."

# Create your models here.
